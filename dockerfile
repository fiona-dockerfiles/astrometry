FROM ubuntu:20.10

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update &&\
    apt-get install -y \
    astrometry.net \
    astrometry-data-tycho2

RUN sed -i 's/cpulimit 300/cpulimit 9999999999999/' /etc/astrometry.cfg

ENTRYPOINT solve-field